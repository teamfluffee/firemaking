package scripts.modules.firemaking;

import scripts.modules.firemaking.data.FiremakingLane;
import scripts.modules.firemaking.nodes.decisionnodes.ShouldWalkToLane;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;

public class Firemaking implements TreeMission {

    public Firemaking(String logName, FiremakingLane lane) {
        BagSingleton.getInstance().addOrUpdate("fmTinderbox", new InteractableItem("Tinderbox", "Use", Interactable.DEFAULT_ID, 1));
        BagSingleton.getInstance().addOrUpdate("fmLogs", new InteractableItem(logName, "Use", Interactable.DEFAULT_ID, 1));
        BagSingleton.getInstance().addOrUpdate("fmLane", lane);
    }

    public Firemaking(InteractableItem logName, FiremakingLane lane) {
        BagSingleton.getInstance().addOrUpdate("fmTinderbox", new InteractableItem("Tinderbox", "Use", Interactable.DEFAULT_ID, 1));
        BagSingleton.getInstance().addOrUpdate("fmLogs", logName);
        BagSingleton.getInstance().addOrUpdate("fmLane", lane);
    }

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToLane();
    }

    @Override
    public String getMissionName() {
        return "Firemaking";
    }

    @Override
    public String getBagId() {
        return BagIds.FIREMAKING.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Inventory.inventoryContains((Interactable) BagSingleton.getInstance().get("fmLogs")) &&
                Inventory.inventoryContains((InteractableItem) BagSingleton.getInstance().get("fmTinderbox"));
    }

    @Override
    public boolean isMissionCompleted() {
        return Inventory.getCount((Interactable) BagSingleton.getInstance().get("fmLogs")) == 0 ||
                ((FiremakingLane) BagSingleton.getInstance().get("fmLane")).getCurrentTile() == null;
    }
}
