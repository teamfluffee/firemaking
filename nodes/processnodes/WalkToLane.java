package scripts.modules.firemaking.nodes.processnodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Walking;
import scripts.modules.firemaking.data.FiremakingLane;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class WalkToLane extends ProcessNode {

    @Override
    public String getStatus() {
        return "Walking to Lane";
    }

    @Override
    public void execute() {
        Walking.walkTo(((FiremakingLane) BagSingleton.getInstance().get("fmLane")).getCurrentTile());
        Timing.waitCondition(Conditions.nearTile(0, ((FiremakingLane) BagSingleton.getInstance().get("fmLane")).getCurrentTile()), General.random(3000, 5000));
    }
}