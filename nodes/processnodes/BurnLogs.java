package scripts.modules.firemaking.nodes.processnodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Login;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSPlayer;
import scripts.modules.firemaking.data.FiremakingLane;
import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.antiban.AntibanUtilities;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ProcessNode;
import scripts.fluffeesapi.utilities.Conditions;

public class BurnLogs extends ProcessNode {

    @Override
    public String getStatus() {
        return "Burning Logs";
    }

    @Override
    public void execute() {
        if (Game.getItemSelectionState() == 1 &&
                Game.getSelectedItemName().equals(((InteractableItem) BagSingleton.getInstance().get("fmTinderbox")).getName())) {
            RSItem logs = Inventory.findFirst((InteractableItem) BagSingleton.getInstance().get("fmLogs"));
            if (logs == null) {
                return;
            }

            int firemakingXp = Skills.getXP(Skills.SKILLS.FIREMAKING);
            if (Clicking.click("Use" , logs) && Timing.waitCondition(Conditions.startedAnimation(733), General.random(3000, 5000))) {
                AntiBanSingleton.get().generateSupportingTrackerInfo(BagSingleton.getInstance().get("fmAverageTime", 300), false);
            } else {
                return;
            }

            RSPlayer rsPlayer = Player.getRSPlayer();
            long startedAction = System.currentTimeMillis();
            while (Login.getLoginState() == Login.STATE.INGAME &&
                    (firemakingXp == Skills.getXP(Skills.SKILLS.FIREMAKING) ||
                            (rsPlayer.getOrientation() < 1500 && rsPlayer.getOrientation() > 1600))) {
                General.sleep(200, 300);
                AntiBanSingleton.get().resolveTimedActions();
            }
            long actionTime = System.currentTimeMillis() - startedAction;

            AntiBanSingleton.get().setLastReactionTime(AntiBanSingleton.get().generateReactionTime((int) actionTime, false));

            BagSingleton.getInstance().addOrUpdate("fmNumberWaits",
                    BagSingleton.getInstance().get("fmNumberWaits", 0) + 1);
            BagSingleton.getInstance().addOrUpdate("fmAverageTime", AntibanUtilities.addWaitTime(
                    (int) actionTime,
                    BagSingleton.getInstance().get("fmNumberWaits"),
                    BagSingleton.getInstance().get("fmAverageTime", 300)
            ));

            AntiBanSingleton.get().setPrintDebug(true);
            AntiBanSingleton.get().setClientDebug(true);
            AntiBanSingleton.get().sleepReactionTime();
            ((FiremakingLane) BagSingleton.getInstance().get("fmLane")).setNextTile();
        } else {
            RSItem tinderbox = Inventory.findFirst((InteractableItem) BagSingleton.getInstance().get("fmTinderbox"));
            if (tinderbox == null) {
                return;
            }
            Clicking.click("Use", tinderbox);
            Timing.waitCondition(() -> {
                General.sleep(200, 400);
                return Game.getItemSelectionState() == 1;
            }, General.random(3000, 5000));
        }
    }
}