package scripts.modules.firemaking.nodes.decisionnodes;

import org.tribot.api2007.Player;
import scripts.modules.firemaking.data.FiremakingLane;
import scripts.modules.firemaking.nodes.processnodes.BurnLogs;
import scripts.modules.firemaking.nodes.processnodes.WalkToLane;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWalkToLane extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !((FiremakingLane) BagSingleton.getInstance().get("fmLane")).getCurrentTile().equals(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLane());
        setFalseNode(new BurnLogs());
    }

}
