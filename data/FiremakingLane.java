package scripts.modules.firemaking.data;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Objects;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public enum FiremakingLane {

    RIMMINGTON_WILLOWS (new RSTile(2974, 3195, 0), new RSTile(3002, 3195, 0));

    private LinkedList<RSTile> laneTiles; //First tile in list is the last tile, last tile is the starting tile of the lane.
    private RSTile startingTile, endingTile, currentTile;
    private Iterator<RSTile> iterator;

    FiremakingLane(LinkedList<RSTile> laneTiles) {
        this.startingTile = laneTiles.getLast();
        this.endingTile = laneTiles.getFirst();
        this.laneTiles = laneTiles;
        this.iterator = laneTiles.descendingIterator();
        this.currentTile = startingTile;
    }

    FiremakingLane(RSTile endingTile, RSTile startingTile) {
        this.startingTile = startingTile;
        this.endingTile = endingTile;
        this.laneTiles = null;
        this.iterator = new LaneIterator(this.startingTile, this.endingTile);
        this.currentTile = startingTile;
    }

    public void setNextTile() {
        if (iterator.hasNext()) {
            do {
                try {
                    currentTile = iterator.next();
                } catch (NoSuchElementException e) {
                    currentTile = null;
                }
            } while (!isTileValid(currentTile));
        } else {
            currentTile = null;
        }
    }

    public RSTile getCurrentTile() {
        if (!isTileValid(currentTile)) {
            setNextTile();
        }
        return currentTile;
    }

    public boolean isTileValid(Positionable tile) {
        return Objects.getAt(tile, (t -> t.getType() != RSObject.TYPES.FLOOR)).length == 0;
    }

    private class LaneIterator implements Iterator {

        private RSTile lastTile, startingTile, endingTile;

        public LaneIterator(RSTile startingTile, RSTile endingTile) {
            this.startingTile = startingTile;
            this.endingTile = endingTile;
            this.lastTile = startingTile;
        }

        @Override
        public boolean hasNext() {
            return lastTile.getX() > this.endingTile.getX();
        }

        @Override
        public RSTile next() {
            if (!hasNext()) {
                return null; //Return null instead of modifying lastTile to ensure the same call can be ran multiple times without issue.
                //Making lastTile null would result in weird issues.
            } else {
                lastTile = lastTile.translate(-1, 0);
            }
            return lastTile;
        }
    }
}
