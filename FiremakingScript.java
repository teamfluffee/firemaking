package scripts.modules.firemaking;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import scripts.modules.firemaking.data.FiremakingLane;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Tools",
        name        = "Fluffees Firemaking",
        description = "We're building it up, to burn it down.",
        gameMode = 1)

public class FiremakingScript extends MissionScript implements Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#ff8c42"), "Firemaking")
            .addField("Version", Double.toString(1.00))
            .build();

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public Mission getMission() {
        return new Firemaking("Willow logs", FiremakingLane.RIMMINGTON_WILLOWS);
    }
}
